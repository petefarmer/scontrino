### Status
![alt text](assets/image/status.png)
![alt text](assets/image/legend.png)

---
![alt text](assets/image/0-image-capture.png)
### 0. Image capture
- as email attachment (MVP v0.1 -- DONE.) 
- as SMS attachment (Production v1.x) |
- from native iOS and Android apps (Production v1.x) |

---
![alt text](assets/image/1-image-storage.png)
### 1. Receive email, store in cloud 
- Currently receiving email, parsing attachments. 
- DONE

---
![alt text](assets/image/2-ocr-storage.png)

### 2. OCR process, store in RDB
- Tested OCR package. 
- Costs: free 1000 per month, $1.50 per 1000 after 
- DONE

---
![alt text](assets/image/3-business-logic.png)

### 3. Business logic
- MVP DONE
- TODO: AI logic

---
![alt text](assets/image/3-business-logic.png)

### 4. Presentation layer
- May use Flask. 
- In progress

---
![alt text](assets/image/3-business-logic.png)

### 5. Outputs and Integration
- What is within scope? 

---
### Misc. TODO
- Identify testing framework. (pytest?)
- CI
- Configure development env

---
## Timeline
![alt text](assets/image/timeline.png)

---
## Costs
- Base:
- OCR API: free 1000 per month, $1.50 per 1000 after
- NoSQL storage:
- SQL storage:
