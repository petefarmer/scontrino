// listen for incoming email
// read mail, parse header, store to sql
// read attachment, store to cloud db
// ocr attachment with vision, store to sql
// sql db = scontrino-test
// root password = segreto

package main

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
	"os"
        "io/ioutil"
	"net/http"
	"path/filepath"
	"context"
	"mime"
/*
Not using default mail, preferring emersion version.
//	"net/mail"
*/
	"github.com/emersion/go-message/mail"
	"strings"
	"database/sql"
	"time"
	"reflect"

	"cloud.google.com/go/storage"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/vision/v1"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/file"


	_ "github.com/go-sql-driver/mysql"
	visionx "cloud.google.com/go/vision/apiv1"
	visionpb "google.golang.org/genproto/googleapis/cloud/vision/v1"
)

var db *sql.DB

func main() {
	var (
		connectionName = mustGetenv("CLOUDSQL_CONNECTION_NAME")
		user = mustGetenv("CLOUDSQL_USER")
		password = mustGetenv("CLOUDSQL_PASSWORD")
	)

	var err error
	db, err = sql.Open("mysql", fmt.Sprintf("%s:%s@cloudsql(%s)/scontrino", user, password, connectionName))
	if err != nil {
		// log.Errorf(ctx, "Could not open db: %s", err)
	}

	http.HandleFunc("/", handle)
	appengine.Main()
}

func handle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, world!")
}

func init() {
	http.HandleFunc("/_ah/mail/", incomingMail)
}

func mustGetenv(k string) string {
	v := os.Getenv(k)
	if v == "" {
	//	log.Errorf(ctx, "%s environment variable not set.", k)
	}
	return v
}

func incomingMail(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	defer r.Body.Close()
	var b bytes.Buffer
	var text string
	if _, err := b.ReadFrom(r.Body); err != nil {
		log.Errorf(ctx, "Error reading body: %v", err)
		return
	}

	// mail.CreateReader wants these bytes converted to a string
	j := b.String()
	k := strings.NewReader(j)

	mr, err := mail.CreateReader(k)
	if err != nil {
		log.Errorf(ctx, "Error: %s", err)
	}

	// Get the default storage bucket
	bucketName, err := file.DefaultBucketName(ctx)
	if err != nil {
		log.Errorf(ctx, "failed to get default GCS bucket: %v", err)
	}
	log.Infof(ctx,"bucketName = %v", bucketName)
	// Get a storage client
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Errorf(ctx, "failed to create client: %v", err)
		return
	}
	defer client.Close()

	// Get a bucketHandle
	bucket := client.Bucket(bucketName)
	// TODO: handle error

	// Get an authenticated vision client
	vclient, err := google.DefaultClient(ctx, vision.CloudPlatformScope)
	if err != nil {
		log.Errorf(ctx, "failed to create vision client: %v", err)
		return
	}

	// What's really happening here? Is this a service worker thread?
	// Need to research this.
	service, err := vision.New(vclient)
	if err != nil {
		log.Errorf(ctx, "failed to create service: %v", err)
		return
	}

	// Loop through the parts of the mail message and extract the file(s).
	for {
		p, err := mr.NextPart()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Errorf(ctx, "Error: %s", err)
		}

		h := mr.Header

		from, err := h.AddressList("From")
		if err != nil {
			log.Errorf(ctx, "failed to read email From address: %v", err)
			return
		}

		log.Infof(ctx, "from = %s", from)
		log.Infof(ctx, "type of address= %s", reflect.TypeOf(from))

		name := from[0].Name
		address := from[0].Address

		log.Infof(ctx, "name= %s, address=%s", name, address)

		time := time.Now()
		log.Infof(ctx, "time = %s", time)

		switch h := p.Header.(type) {

		case mail.TextHeader:
			b, _ := ioutil.ReadAll(p.Body)
			log.Infof(ctx, "Got text: %v", string(b))

		case mail.AttachmentHeader:
		// First, get the attachments and save them in a bucket
			filename, _ := h.Filename()
			ext := filepath.Ext(filename)
			log.Infof(ctx, "extension = %s", ext)
			mimetype := mime.TypeByExtension(ext)
			log.Infof(ctx, "mimetype = %s", mimetype)
			filedata, _ := ioutil.ReadAll(p.Body)

			wc := bucket.Object(filename).NewWriter(ctx)

			if _, err := wc.Write([]byte(filedata)); err != nil {
				log.Errorf(ctx, "unable to write to bucket %q, file %q: %v", bucketName, filename, err)
			}
			if err := wc.Close(); err != nil {
				// TODO: Handle error.
			}
			log.Infof(ctx, "wrote file to bucket %q", bucketName)


			// Second, pass the file data to the OCR API (vision)
			switch mimetype {
			case "image/jpeg":
				log.Infof(ctx, "IT's a JPEG")

				req := &vision.AnnotateImageRequest{
					Image: &vision.Image{
						Content: base64.StdEncoding.EncodeToString(filedata),
					},
					Features: []*vision.Feature{
						{
							Type: "TEXT_DETECTION",
						},
					},
				}

				batch := &vision.BatchAnnotateImagesRequest{
					Requests: []*vision.AnnotateImageRequest{req},
				}

				res, err := service.Images.Annotate(batch).Do()
				if err != nil {
					log.Errorf(ctx, "service failed: %v", err)
					return
				}

				if annotations := res.Responses[0].TextAnnotations; len(annotations) > 0 {
					text = annotations[0].Description
					log.Infof(ctx, "text: %q", text)
				}

				writeNewSQLRecord(ctx, name, address, filename, text)
			
			case "application/pdf":
				log.Infof(ctx, "IT's a PDF")
				gcsSourceURI := join("gs://", bucketName, "/", filename)
				gcsDestinationURI := join("gs://", bucketName, "/", filename, ".json")
				client, err := visionx.NewImageAnnotatorClient(ctx)
				if err != nil {
					return 
				}

				request := &visionpb.AsyncBatchAnnotateFilesRequest{
					Requests: []*visionpb.AsyncAnnotateFileRequest{
						{
							Features: []*visionpb.Feature{
								{
									Type: visionpb.Feature_DOCUMENT_TEXT_DETECTION,
								},
							},
							InputConfig: &visionpb.InputConfig{
								GcsSource: &visionpb.GcsSource{Uri: gcsSourceURI},
								MimeType: "application/pdf",
							},
							OutputConfig: &visionpb.OutputConfig{
								GcsDestination: &visionpb.GcsDestination{Uri: gcsDestinationURI},
								BatchSize: 1,
							},

						},
					},
				}
				operation, err := client.AsyncBatchAnnotateFiles(ctx, request)
				if err != nil {
					return 
				}

				resp, err := operation.Wait(ctx)


				log.Infof(ctx, "RESPONSE to pdf: %v", resp)
				newfile := join(filename, ".jsonoutput-1-to-1.json")
				rc, err := bucket.Object(newfile).NewReader(ctx)
				if err != nil {
					log.Errorf(ctx, "can't get newfile from bucket: %v", err)
					return
				}
				defer rc.Close()
				slurp, err := ioutil.ReadAll(rc)
				if err != nil {
					log.Errorf(ctx, "Unable to read data: %v", err)
					return
				}

				t := string(slurp)

				log.Infof(ctx, "slurp = %s", t)

				writeNewSQLRecord(ctx, name, address, filename, t)


			}



			
			/************************************************************
			This section is for example only -- cut from production!

			rows, err := db.Query("SHOW DATABASES")
			if err != nil {
				log.Errorf(ctx, fmt.Sprintf("Could not query db: %v", err), 500)
				return
			}
			defer rows.Close()

			buf := bytes.NewBufferString("Databases:\n")
			for rows.Next() {
				var dbName string
				if err := rows.Scan(&dbName); err != nil {
					http.Error(w, fmt.Sprintf("Could not scan result: %v", err), 500)
					return
				}
				log.Infof(ctx,"buf = %v, dbName = %s",buf, dbName)
			}
			w.Write(buf.Bytes())
			log.Infof(ctx, "buf = %v", buf)
			*/
		}
	}
}

// Third, store the OCR results and mail data in SQL
func writeNewSQLRecord(ctx context.Context, name string, address string, filename string, text string) {

	stmt, err := db.Prepare("INSERT inbox SET name=?, address=?, filename=?, ocr_text=?")

	log.Infof(ctx, "inserted values %s, %s, %s plus the ocr_text into inbox db", name, address, filename)

	spoo, err := stmt.Exec(name, address, filename, text)

	if err != nil {
		log.Errorf(ctx, fmt.Sprintf("spoo = : %v, %v", err, spoo), 500)
		return
	}
}

func join(strs ...string) string {
	var ret string
	for _, str := range strs {
		ret += str
	}
	return ret
}

